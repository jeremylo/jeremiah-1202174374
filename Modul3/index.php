<?php
$name = "Hello World";
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, shrink-to-fit=no"
    />
    <title>TP WAD</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/styles.css" />
  </head>

  <body>
    <div class="container">
      <div class="col-8 offset-2">
        <h1 class="text-center">Kopi Susu Duar</h1>
        <p class="text-center">Best Coffe Shop In Town</p>
        <form method="GET" action="menu.php">
          <div class="form-group">
            <div class="form-row">
              <div class="col">
                <label class="col-form-label">Nama Driver</label>
              </div>
              <div class="col">
                <input
                  class="form-control"
                  type="text"
                  required=""
                  name="nama"
                  placeholder="Masukan nama"
                />
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col">
                <label class="col-form-label">Nomor Telepon</label>
              </div>
              <div class="col">
                <input
                  class="form-control"
                  type="number"
                  placeholder="Masukan nomor telepon"
                  name="no_telepon"
                />
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col">
                <label class="col-form-label">Tanggal Pesan</label>
              </div>
              <div class="col">
                <input name="tanggal_pesan" class="form-control" type="date" />
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col">
                <label class="col-form-label">Driver Dari</label>
              </div>
              <div class="col">
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="radio"
                    id="driver"
                    name="driver"
                    value="Gojek"
                  /><label class="form-check-label" for="driver">Gojek</label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="radio"
                    id="formCheck-1"
                    name="driver"
                    value="Grab"
                  /><label class="form-check-label" for="formCheck-1"
                    >Grab</label
                  >
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col">
                <label class="col-form-label">Bawa Kantong Belanja?</label>
              </div>
              <div class="col">
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="checkbox"
                    name="is_kantong"
                    value="ya"
                    id="formCheck-2"
                  /><label class="form-check-label" for="formCheck-2">Ya</label>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col text-center">
                <button
                  class="btn btn-primary"
                  type="submit"
                  style="margin-right: 20px;"
                >
                  Send</button
                ><button class="btn btn-outline-primary" type="reset">
                  Clear
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
