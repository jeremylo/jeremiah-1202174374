<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, shrink-to-fit=no"
    />
    <title>TP WAD</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/styles.css" />
  </head>

  <body>
    <div class="container">
      <div class="row">
        <div class="col">
          <h1 class="text-center">~Data Driver Ojol~</h1>
          <div class="biodata">
            <p>
              Nama: <?php echo $_GET["nama"] ?>
            </p>
            <p>
              No telepon: <?php echo $_GET["no_telepon"] ?>
            </p>
            <p>
              <?php echo $_GET["tanggal_pesan"] ?>
            </p>
            <p>
              <?php echo $_GET["driver"] ?>
            </p>
            <p>
              <?php if (isset($_GET["is_kantong"])): ?>
                  Bawa kantong gan
                  <?php else: ?>
                  Tidak bawa kantong
              <?php endif;?>
            </p>
          </div>

        </div>
        <div class="col data-ojol" style="text-align: center">
          <h1 class="text-center">~Menu~</h1>
          <p>Pilih Menu</p>

        <form method="GET" action="nota.php">
        <div class="form-group">
            <div class="row">
              <div class="col">
                <input
                  class="form-check-input"
                  type="checkbox"
                  name="es_teh"
                  value="30000"
                /><label class="form-check-label" for="formCheck-2"
                  >Es Teh Susu</label
                >
              </div>
              <div class="col">
                <span>Rp 30.000</span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col">
                <input
                  class="form-check-input"
                  type="checkbox"
                  name="chat_time"
                  value="50000"
                /><label class="form-check-label" for="formCheck-2"
                  >Chattime</label
                >
              </div>
              <div class="col">
                <span>Rp 50.000</span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col">
                <input
                  class="form-check-input"
                  type="checkbox"
                  name="es_coklat"
                  value="90000"
                /><label class="form-check-label" for="formCheck-2"
                  >Es Coklat</label
                >
              </div>
              <div class="col">
                <span>Rp 90.000</span>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col">
                <label for="">Nomor Order</label>
              </div>
              <div class="col">
                <input
                  class="form-control"
                  name="nomor_order"
                  type="number"
                  placeholder="Masukan nomor order"
                />
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col">
                <label for="">Nama Pemesanan</label>
              </div>
              <div class="col">
                <input
                  name="nama"
                  class="form-control"
                  type="text"
                  placeholder="Masukan alamat"
                />
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col">
                <label for="">Email</label>
              </div>
              <div class="col">
                <input
                  name="email"
                  class="form-control"
                  type="text"
                  placeholder="Masukan alamat"
                />
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col">
                <label for="">Alamat</label>
              </div>
              <div class="col">
                <input
                  name="alamat"
                  class="form-control"
                  type="text"
                  placeholder="Masukan alamat"
                />
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col">
                <label for="">Member</label>
              </div>
              <div class="col">
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="radio"
                    name="is_member"
                    value="Ya"
                  /><label class="form-check-label" for="formCheck-1">Ya</label>
                </div>
                <div class="form-check">
                  <input
                    class="form-check-input"
                    type="radio"
                    name="is_member"
                    value="Tidak"
                  /><label class="form-check-label" for="formCheck-1"
                    >Tidak</label
                  >
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row">
              <div class="col">
                <label for="">Metode Pembayaran</label>
              </div>
              <div class="col">
                <select name="pembayaran" class="form-control">
                  <option value="cash">Cash</option>
                  <option value="OVO">OVO</option>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <button class="btn btn-primary" type="submit">
              Cetak Struk
            </button>
          </div>
        </form>
        </div>
      </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
