<?php

$data = ["es_teh", "chat_time", "es_coklat"];

$total = null;

foreach ($data as $value) {
    if (isset($_GET[$value])) {
        $total += $_GET[$value];
    }
}

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1.0, shrink-to-fit=no"
    />
    <title>TP WAD</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="assets/css/styles.css" />
  </head>

  <body>
    <div class="container">
      <div class="col-8 offset-2">
        <h1 class="text-center">Transaksi Pemesanan</h1>
        <p class="text-center">
          Terimakasih telah berbelanja di Kopi Susu Duar
        </p>

        <h1 class="text-center">Rp <?php echo $total ?></h1>

        <form>
          <div class="form-group">
            <div class="form-row">
              <div class="col">ID</div>
              <div class="col"><?php echo $_GET["nomor_order"] ?></div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col">Nama</div>
              <div class="col"><?php echo $_GET["nama"] ?></div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col">Email</div>
              <div class="col"><?php echo $_GET["email"] ?></div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col">Alamat</div>
              <div class="col"><?php echo $_GET["alamat"] ?></div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col">Member</div>
              <div class="col">
                <?php echo $_GET["is_member"] ?>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-row">
              <div class="col">Pembayaran</div>
              <div class="col">
                <?php echo $_GET["pembayaran"] ?>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
