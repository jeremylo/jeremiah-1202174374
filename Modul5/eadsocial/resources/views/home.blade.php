@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
           @foreach ($posts as $post)
                <div class="card">
                <div class="card-header">
                    <img style="width: 30px; height: 30px; border-radius: 50%" class="img-fluid" src="{{ $post->image }}" alt="">
                    {{ $post->user->name }}
                </div>

                <div class="card-body">
                    <img class="img-fluid" src="{{ $post->image }}" alt="">
                </div>

                <div class="card-footer">{{ $post->caption }}</div>
            </div>
           @endforeach
        </div>
    </div>
</div>
@endsection
