<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;


class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        return DB::table('users')->insert([
            'name' => $faker->name,
            'email' => $faker->email,
            'avatar' => $faker->email,
            'password' => bcrypt('password'),
        ]);
    }
}
