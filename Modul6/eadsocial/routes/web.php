<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'PostsController@index')->name('home');
Route::get('/home', 'PostsController@index')->name('home');
Route::get("posts/likes/{id}", 'PostsLikesController@store')->middleware('auth');
Route::get("/profile/{id}", 'ProfileController@index')->middleware('auth');
Route::resource('posts/comments', 'PostsCommentsController')->middleware('auth');
Route::resource('posts', 'PostsController')->middleware('auth');
Route::get('edit/profile', 'ProfileController@edit')->middleware('auth');
Route::post('edit/profile', 'ProfileController@update')->middleware('auth');
