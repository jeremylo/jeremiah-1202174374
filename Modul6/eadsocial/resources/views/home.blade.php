@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
           @foreach ($posts as $post)
                <div class="card" style="margin-bottom: 10px">
                <div class="card-header">
                    <img style="width: 30px; height: 30px; border-radius: 50%" class="img-fluid" src="/img-profile/{{ $post->user->avatar }}" alt="">
                    <a href="/profile/{{ $post->user->id }}">
                        {{ $post->user->name }}
                    </a>
                </div>

                <div class="card-body">
                   <a href="posts/{{ $post->id }}">
                    <img class="img-fluid" src="/img/{{ $post->image }}" alt="">
                   </a>
                </div>

                <div class="card-footer">
                    {{ $post->caption }}
                    <p>
                    <a href="/posts/likes/{{$post->id}}">
                            ❤️ {{ count($post->likes) }}
                    </a>
                    </p>
                    <p>
                        💬 {{ count($post->comments) }}
                    </p>
                    <hr>
                    @foreach ($post->comments as $comment)
                        <p>{{ $comment->user->name }} {{ $comment->comment }}</p>
                    @endforeach
                    <hr>
                    <form method="POST" action="/posts/comments">
                       @csrf
        
                       <div class="form-group">
                          <label for="exampleFormControlTextarea1">Komenin gan</label>
                          <textarea class="form-control" name="comment" rows="3"></textarea>
                       </div>
        
                       <input value="{{ $post->id }}" name="posts_id" type="hidden">
                       <button type="submit" class="btn btn-primary">Kirim</button>
                    </form>
                </div>
            </div>
           @endforeach
        </div>
    </div>
</div>
@endsection
