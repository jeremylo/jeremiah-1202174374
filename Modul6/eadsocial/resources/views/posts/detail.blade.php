@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
       <div class="col-md-7">
          <img class="img-fluid" src="/img/{{$post->image}}">
       </div>
        <div class="col-md-3">
            <div class="row">
               <div style="margin-right: 10px">
                  <img style="width: 30px; height: 30px; border-radius: 50%" class="img-fluid" src="/img-profile/{{ $post->user->avatar }}" alt="">
               </div>
               <a href="/profile/{{ $post->user->id }}">
                  {{ $post->user->name }}
              </a>
            </div>
            <hr>
            <p>{{$post->caption}}</p>
            <p>
               <a href="/posts/likes/{{$post->id}}">
                     ❤️ {{ count($post->likes) }}
               </a>
            </p>
            <p>
               💬 {{ count($post->comments) }}
            </p>
            <hr>
            @foreach ($post->comments as $comment)
               <p>{{ $comment->user->name }} {{ $comment->comment }}</p>
            @endforeach
            <hr>
            <form method="POST" action="/posts/comments">
               @csrf

               <div class="form-group">
                  <label for="exampleFormControlTextarea1">Komenin gan</label>
                  <textarea class="form-control" name="comment" rows="3"></textarea>
               </div>

               <input value="{{ $post->id }}" name="posts_id" type="hidden">
               <button type="submit" class="btn btn-primary">Kirim</button>
            </form>
        </div>
    </div>
</div>
@endsection
