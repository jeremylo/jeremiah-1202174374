@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
         <h3>Add New Post</h3>

         <form method="POST" action="/edit/profile" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">Title</label>
              <input value="{{ $profile->title }}" name="title" type="text" class="form-control" placeholder="Title bro">
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Description</label>
               <input value="{{ $profile->description }}" name="description" type="text" class="form-control" placeholder="Desc bro">
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">URL</label>
               <input value="{{ $profile->url }}" name="url" type="text" class="form-control" placeholder="URL bro">
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Avatar</label>
               <input name="image" type="file" class="form-control-file" id="exampleFormControlFile1">
             </div>

            <button type="submit" class="btn btn-primary">Edit</button>
          </form>
         
        </div>
    </div>
</div>
@endsection
