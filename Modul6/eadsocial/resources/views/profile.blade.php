@extends('layouts.app')

@section('content')

<div class="container">
      <div class="row">
         <div class="col-2">
            <img style="border-radius: 50%" class="img-fluid img-circle" src="/img-profile/{{ $profile->avatar }}" alt="">
         </div>
         <div class="col-8">
            <h3>{{ $profile->name }}</h3>
            <b>{{ count($profile->posts) }} Posts</b>
            <p>{{ $profile->description }}</p>
            <a href="/edit/profile/">Edit</a>
         </div>
      </div>

      <hr>

      <div class="row">
         @foreach ($profile->posts as $post)
         <div class="col">
            <div class="card">
               <div class="card-body">
                  <img class="img-fluid" src="/img/{{ $post->image }}" alt="">
               </div>
            </div>
         </div>
             
         @endforeach
      </div>
</div>
@endsection
