@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
         <h3>Add New Post</h3>

         <form method="POST" action="/posts" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="exampleInputEmail1">Post caption</label>
              <input name="caption" type="text" class="form-control" placeholder="Post caption bro">
            </div>
            <div class="form-group">
               <label for="exampleInputEmail1">Post image</label>
               <input name="image" type="file" class="form-control-file" id="exampleFormControlFile1">
             </div>

            <button type="submit" class="btn btn-primary">Add new post</button>
          </form>
         
        </div>
    </div>
</div>
@endsection
