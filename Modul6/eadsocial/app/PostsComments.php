<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostsComments extends Model
{
    protected $fillable = ["comment", "user_id", "posts_id"];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
