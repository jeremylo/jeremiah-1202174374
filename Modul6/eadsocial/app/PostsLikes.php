<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostsLikes extends Model
{
    protected $fillable = ["user_id", "posts_id"];
}
