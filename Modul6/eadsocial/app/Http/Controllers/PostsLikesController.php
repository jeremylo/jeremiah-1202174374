<?php

namespace App\Http\Controllers;

use App\PostsLikes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PostsLikesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $posts = PostsLikes::firstOrCreate([
            "user_id" => Auth::id(),
            "posts_id" => $id,
        ]);

        if (!$posts->wasRecentlyCreated) {
            $posts->delete();
        }

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostsLikes  $postsLikes
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostsLikes $postsLikes)
    {
        //
    }
}
