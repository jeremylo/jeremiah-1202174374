<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index($id)
    {
        $profile = User::with('posts')->findOrFail($id);

        return view('profile', ["profile" => $profile]);
    }

    public function edit()
    {
        $userId = Auth::id();

        $profile = User::findOrFail($userId);

        return view('edit_profile', ["profile" => $profile]);
    }

    public function update(Request $request)
    {
        $file = $request->file('image');

        $file->move("img-profile", $file->getClientOriginalName());

        $userId = Auth::id();

        $user = User::find($userId);
        $user->title = $request->input('title');
        $user->description = $request->input('description');
        $user->url = $request->input('url');
        $user->avatar = $file->getClientOriginalName();

        $user->save();

        return redirect('/');
    }
}
