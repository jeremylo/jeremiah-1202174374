<?php

namespace App\Http\Controllers;

use App\PostsComments;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class PostsCommentsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        PostsComments::create([
            "user_id" => Auth::id(),
            "posts_id" => $request->input("posts_id"),
            "comment" => $request->input("comment"),
            "image" => "url.png"
        ]);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostsComments  $postsComments
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostsComments $postsComments)
    {
        //
    }
}
