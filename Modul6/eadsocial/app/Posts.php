<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    protected $fillable = ["user_id", "caption", "image"];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function likes()
    {
        return $this->hasMany('App\PostsLikes');
    }

    public function comments()
    {
        return $this->hasMany('App\PostsComments');
    }
}
