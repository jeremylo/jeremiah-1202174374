<?php
require_once('config/Database.php');

class CartModel
{
   private $table = "cart";
   private $database;

   public function __construct()
   {
      $this->database = new Database();
   }

   public function selectCart(array $payload): array
   {
      $query = "SELECT * FROM $this->table WHERE user_id = :user_id ORDER BY id DESC";

      $cart = $this->database->prepareAndExecute($query, $payload);

      return $cart->fetchAll();
   }

   public function insertCart(array $payload)
   {
      $query = "INSERT INTO $this->table (user_id, product, price) VALUES (:user_id, :product, :price)";

      return $this->database->prepareAndExecute($query, $payload);
   }

   public function deleteCart(array $payload)
   {
      $query = "DELETE FROM $this->table WHERE user_id = :user_id AND id = :product";

      return $this->database->prepareAndExecute($query, $payload);
   }
}
