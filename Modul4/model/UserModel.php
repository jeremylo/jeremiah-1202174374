<?php
require_once('config/Database.php');

class UserModel
{
    private $table = "user";
    private $database;

    public function __construct()
    {
        $this->database = new Database();
    }

    public function login(array $payload)
    {
        $query = "SELECT * FROM $this->table WHERE email = :email LIMIT 1";

        $user = $this->database->prepareAndExecute($query, ["email" => $payload["email"]]);

        return $user->fetch();
    }

    public function register(array $payload)
    {
        $query = "INSERT INTO $this->table (email, username, password, mobile_number) VALUES (:email, :username, :password, :mobilenumber)";

        return $this->database->prepareAndExecute($query, $payload);
    }

    public function getAllUser()
    {
        $user = $this->database->query("SELECT * FROM $this->table");

        return $user->fetchAll();
    }

    public function getUser($payload)
    {
        $user = $this->database->prepareAndExecute("SELECT * FROM $this->table WHERE id = :id LIMIT 1", $payload);

        return $user->fetch();
    }

    public function updateProfile($payload): bool
    {
        $query = "UPDATE $this->table SET email = :email, username = :username, password = :password, mobilenumber = :mobilenumber";

        return $this->database->prepareAndExecute($query, $payload);
    }
}
