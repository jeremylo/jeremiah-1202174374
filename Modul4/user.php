<?php
session_start();
require_once 'handler/UserHandler.php';

$user = new UserHandler();

if (isset($_GET["login"])) $user->login();

if (isset($_GET["register"])) $user->register();

if (isset($_GET["update"])) $user->updateProfile();

if (isset($_GET["logout"])) session_destroy();

header('Location: ' . "index.php");
