<?php
require_once 'template/header.php';

$cart = new CartHandler();
$getCart = $cart->getCart();

if (isset($_GET["name"]) && isset($_GET["price"])) $cart->addCart();
if (isset($_GET["delete"])) $cart->deleteCart();

$getCart = $cart->getCart();
?>

<div class="container" style="margin-top: 15px">
   <div class="card">
      <div class="card-header">
         Cart
      </div>
      <table class="card-table table">
         <thead>
            <tr>
               <th scope="col">#</th>
               <th scope="col">Product</th>
               <th scope="col">Price</th>
               <th scope="col">Action</th>
            </tr>
         </thead>
         <tbody>
            <?php foreach ($getCart["data"] as $cart) : ?>
               <tr>
                  <th scope="row"><?php echo $cart["id"]; ?></th>
                  <td><?php echo $cart["product"] ?></td>
                  <td><?php echo $cart["price"] ?></td>
                  <td>
                     <a href="cart.php?delete=<?php echo $cart["id"] ?>" class="btn btn-danger">Hapus</a>
                  </td>
               </tr>
            <?php endforeach; ?>
         </tbody>
      </table>
      <div class="card-footer text-muted">
         Total: <?php echo $getCart["count"] ?>
      </div>
   </div>
</div>

<?php require_once 'template/footer.php'; ?>