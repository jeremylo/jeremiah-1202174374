<?php require_once 'template/header.php'; ?>

<div class="container" style="margin-top: 15px">
  <div class="row">
    <div class="col">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Learn Go</h5>
          <p class="card-text">
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <a href="cart.php?name=Learn Go&price=90000" class="btn btn-primary btn-block">Buy</a>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Learn Node.js</h5>
          <p class="card-text">
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <a href="cart.php?name=Learn Node.JS&price=90000" class="btn btn-primary btn-block">Buy</a>
        </div>
      </div>
    </div>
    <div class="col">
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">Why PHP Sucks</h5>
          <p class="card-text">
            Some quick example text to build on the card title and make up the
            bulk of the card's content.
          </p>
          <a href="cart.php?name=Why PHP Sucks&price=90000" class="btn btn-primary btn-block">Buy</a>
        </div>
      </div>
    </div>
  </div>
</div>

<?php require_once 'template/footer.php'; ?>