<?php
require_once('model/CartModel.php');
require_once('UserHandler.php');

class CartHandler
{
   public $model;

   public function __construct()
   {
      if (!UserHandler::checkSession()) die("You must login first!");

      $this->model = new CartModel();
   }

   public function addCart(): bool
   {
      $payload = [
         "user_id" => $_SESSION["id"],
         "product" => $_GET["name"],
         "price" => $_GET["price"],
      ];

      if (!$this->model->insertCart($payload)) die("Failed to insert cart to the db");

      return true;
   }

   public function deleteCart(): bool
   {
      $payload = [
         "user_id" => $_SESSION["id"],
         "product" => $_GET["delete"],
      ];

      if (!$this->model->deleteCart($payload)) die("Failed to delete cart from db");

      return true;
   }

   private function sumCart($data): int
   {
      $total = 0;

      foreach ($data as $key => $value) {
         $total += $value["price"];
      }

      return $total
   }

   public function getCart(): array
   {
      $payload = ["user_id" => $_SESSION["id"]];

      $data = $this->model->selectCart($payload);

      return [
         "data" => $this->model->selectCart($payload),
         "count" => $this->sumCart($data)
      ];
   }
}
