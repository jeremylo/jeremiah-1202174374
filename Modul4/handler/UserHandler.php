<?php
require_once('model/UserModel.php');

class UserHandler
{
    public $model;

    public function __construct()
    {
        $this->model = new UserModel();
    }

    public function setSession($user)
    {
        $_SESSION["id"] = $user["id"];
        $_SESSION["username"] = $user["username"];
        $_SESSION["email"] = $user["email"];
    }

    // TODO:: better way to do this
    public static function checkSession(): bool
    {
        if (!isset($_SESSION["username"])) return false;

        return true;
    }

    private function verifyPassword($payload, $user): bool
    {
        return password_verify($payload["password"], $user["password"]);
    }

    public function login(): bool
    {
        $payload = [
            "email" => $_POST["email"],
            "password" => $_POST["password"],
        ];

        $user = $this->model->login($payload);

        if (!$user) return false;
        if (!$this->verifyPassword($payload, $user)) return false;

        $this->setSession($user);

        return true;
    }

    public function register()
    {
        $payload = [
            "email" => $_POST["email"],
            "username" => $_POST["username"],
            "password" => password_hash($payload["password"], PASSWORD_DEFAULT),
            "mobilenumber" => $_POST["mobilenumber"],
        ];

        return $this->model->register($payload);
    }

    public function updateProfile(): bool
    {
        $payload = [
            "email" => $_POST["email"],
            "username" => $_POST["username"],
            "password" => password_hash($_POST["password"], PASSWORD_DEFAULT),
            "mobilenumber" => $_POST["mobilenumber"],
        ];

        return $this->model->updateProfile($payload);
    }

    public function getUser()
    {
        if (self::checkSession())
            return $this->model->getUser(["id" => $_SESSION["id"]]);
    }

    public function getAllUser()
    {
        return $this->model->getAllUser();
    }
}
