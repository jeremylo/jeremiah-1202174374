<?php

class Database
{
    private $dbName;

    public function __construct()
    {
        $this->dbName = "crud-cart-session";
    }

    public function connection()
    {
        return new PDO("mysql:host=localhost;dbname=$this->dbName", "root", "root");
    }

    public function prepareAndExecute(string $query, array $payload)
    {
        $st = $this->connection()->prepare($query);
        $st->execute($payload);

        return $st;
    }

    public function query(string $query)
    {
        return $this->connection()->query($query);
    }
}
