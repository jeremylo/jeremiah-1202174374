$(document).ready(function() {
  $("#question").submit(function(e) {
    e.preventDefault();

    const identity = {
      name: $("#inputNama").val(),
      nim: $("#inputNim").val()
    };

    if (identity.name === "" || identity.nim === "") {
      alert("Maaf harus mengisi biodata");
    } else {
      countScore(identity);
    }
  });
});

function questionAndAnswer() {
  return [
    {
      name: "soal_1",
      value: "B"
    },
    {
      name: "soal_2",
      value: "B"
    },
    {
      name: "soal_3",
      value: "B"
    },
    {
      name: "soal_4",
      value: "B"
    },
    {
      name: "soal_5",
      value: "B"
    }
  ];
}

function countScore(identity) {
  let correctAnswer = 0;

  const data = questionAndAnswer();

  data.forEach(qa => {
    const value = $(`input[name=${qa.name}]:checked`).val();

    if (value === qa.value) {
      correctAnswer += 1;
    }
  });

  const score = correctAnswer * 20;

  alert(`
   Nama: ${identity.name}
   NIM: ${identity.nim}
   Score: ${score}
  `);
}
